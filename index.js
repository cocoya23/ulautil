const fs = require("fs");
const request = require("request");
const stream = require('stream');
const flow = require("xml-flow");

let init = "http://misrecibos.ula.edu.mx/pronom_recibos_cfdi/ula/";
let folder = "/home/enrique/Downloads/ula/";
let exp = /.\w+$/;
let rfcExp = /[A-Z]{4}\d{6}\w{3}/;

//search(init);
//searchRFCs();
uniqueRFC()

function searchRFCs() {
	fs.readFile('/home/enrique/Downloads/ula/file_names.txt', (err, file) => {
		file.toString().split('\n').forEach((line) => {
			let rfc = line.toString().match(rfcExp);
			if (rfc) fs.appendFileSync("/home/enrique/Downloads/ula/rfcs.txt", rfc + "\n");
		});
	})
}

function uniqueRFC() {
	
	fs.readFile('/home/enrique/Downloads/ula/rfcs.txt', (err, file) => {
		let rfcsSet = new Set(file.toString().split('\n'));
		let rfcs = Array.from(rfcsSet).sort();
		rfcs.forEach((rfc) => {
			fs.appendFileSync("/home/enrique/Downloads/ula/urfcs.txt", rfc.toString() + "\n");
		});
	})
}

function search(url) {
	if (exp.test(url)) saveFileName(url);
	else getListItems(url);
}

function downloadFile(url) {
	let directory = folder + url.substr(url.lastIndexOf("/") + 1);
	request(url).pipe(fs.createWriteStream(directory));
}

function saveFileName(url) {
	fs.appendFileSync('/home/enrique/Downloads/ula/file_names.txt', url.substr(url.lastIndexOf("/") + 1) + "\n");
}

function getListItems(url) {
	
	request({
		url,
		method: "GET"
	}, (err, res, body) => {
		let Readable = stream.Readable;
		let s = new Readable();
		s._read = function noop() {
		}; // redundant? see update below
		s.push(body);
		s.push(null);
		
		let xmlStream = flow(s);
		xmlStream.on('tag:li', function (li) {
			if (li.a["$text"] !== "Parent Directory") {
				search(url + li.a["$attrs"].href)
			}
		});
	});
}