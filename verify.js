const fs = require('fs');
const async = require('async');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
	accessKeyId: "AKIAJKAD2NYOKOC2RCCA",
	secretAccessKey: "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"
});

const home = "/run/media/enrique/Data/CFDI";

let localFiles = [];
let s3Files = [];

console.log("Reading local...");
readFiles(home);
let localSize = localFiles.length;
console.log(localSize);

console.log("Reading s3...");
readS3(null,() => {
	let s3Size = s3Files.length;
	console.log("Begin compare");
	console.log(s3Size);
	compareFiles();
});



function readFiles(path) {
	if (fs.statSync(path).isDirectory()) {
		let files = fs.readdirSync(path);
		files.forEach((file) => {
			readFiles(path + "/" + file);
		});
	} else {
		localFiles.push(path.replace(home + '/', ''));
	}
}


function readS3(nextToken,finish) {
	
	let params = {
		Bucket: 'ulacfdi',
		MaxKeys: 100000
	};
	
	if(nextToken) params.ContinuationToken=nextToken;
	
	s3.listObjectsV2(params, function (err, response) {
		if (err) console.error(err);
		else {
			response.Contents.forEach((object) => {s3Files.push(object.Key)});
			console.log(s3Files.length);
			if(response.IsTruncated){
				readS3(response.NextContinuationToken,finish);
			} else {
				finish();
			}
		}
	});
	
}


function uploadFile(path, next) {
	console.log("Uploading: " + path);
	fs.readFile(home+"/"+path, (err, data) => {
		if (err) return console.error(err);
		s3.putObject({
			Bucket: 'ulacfdi',
			Key: path,
			Body: data
		}, function (er, response) {
			if (er) console.error(er);
			else console.log("Success: " + path);
			next();
		});
	});
	
}

function compareFiles() {
	let missing = localFiles.filter((file)=>{return !s3Files.includes(file)});
	console.log("Missing");
	console.log(missing.length);
	async.eachLimit(missing,10,(file,next) => {
		uploadFile(file,next);
	});
}